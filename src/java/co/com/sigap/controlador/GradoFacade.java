/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.sigap.controlador;

import co.com.sigap.entidades.Grado;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Auris
 */
@Stateless
public class GradoFacade extends AbstractFacade<Grado> {

    @PersistenceContext(unitName = "sigapPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public GradoFacade() {
        super(Grado.class);
    }
    
    public List obtenerCantidadEstudiantesGrado (String grado) {
        Query q=em.createNativeQuery("select a.nombre_asignatura, count(e.cedula_estudiante) from estudiante e join grado g on e.grado = g.id_grado join asignatura a on g.id_grado = a.grado where nombre_grado = '"+grado+"' group by a.nombre_asignatura");
        List<Object[]> lista = q.getResultList();
        return lista;
    }
}
