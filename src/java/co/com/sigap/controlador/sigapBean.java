/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.sigap.controlador;

import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import org.primefaces.model.chart.PieChartModel;


/**
 *
 * @author DANIEL
 */
@Named(value = "sigapBean")
@SessionScoped
public class sigapBean implements Serializable {

    /**
     * Creates a new instance of sigapBean
     */
    public sigapBean() {
    }
    
    @EJB
    private GradoFacade conGradoFacade;
 
    private PieChartModel pieModel1;
 
    
 
    public PieChartModel getPieModel1() {
        return pieModel1;
    }
     
    private void createPieModels() {
        createPieModel1();
    }
 
    
    private String gradoSeleccionado;

    public String getGradoSeleccionado() {
        return gradoSeleccionado;
    }

    public void setGradoSeleccionado(String gradoSeleccionado) {
        this.gradoSeleccionado = gradoSeleccionado;
    }
    
    
    
    public PieChartModel createPieModel1() {
        pieModel1 = new PieChartModel();
        
        List<Object[]> estudiantesGrado = conGradoFacade.obtenerCantidadEstudiantesGrado(gradoSeleccionado);
        
        for(Object[] item : estudiantesGrado) {
            pieModel1.set(item[0].toString(),Double.parseDouble(item[1].toString()));
        }
        
        pieModel1.setTitle("Estudiantes por Grado");
        pieModel1.setLegendPosition("e");
        pieModel1.setFill(false);
        pieModel1.setShowDataLabels(true);
        pieModel1.setDiameter(150);
        /*
        pieModel1.set("Brand 1", 540);
        pieModel1.set("Brand 2", 325);
        pieModel1.set("Brand 3", 702);
        pieModel1.set("Brand 4", 421);
         
        pieModel1.setTitle("Simple Pie");
        pieModel1.setLegendPosition("w");*/
        
        return pieModel1;
    }
}
