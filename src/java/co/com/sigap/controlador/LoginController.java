/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.sigap.controlador;

import co.com.sigap.controlador.util.JsfUtil;
import co.com.sigap.entidades.Usuario;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import java.io.Serializable;
import javax.ejb.EJB;

/**
 *
 * @author Auris
 */
@Named(value = "loginController")
@SessionScoped
public class LoginController implements Serializable {

    private String nombreUsuario = "";
    private String passwordUsuario = "";
    @EJB
    private UsuarioFacade usuFacade;

    public LoginController() {
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getPasswordUsuario() {
        return passwordUsuario;
    }

    public void setPasswordUsuario(String passwordUsuario) {
        this.passwordUsuario = passwordUsuario;
    }

    public String ingresar() {
        Usuario usuEncontrado = usuFacade.obtenerUsuario(this.nombreUsuario);
        if (usuEncontrado != null) {
            if (usuEncontrado.getContrasena().compareTo(passwordUsuario) == 0) {
                return "acceder";
            }
            JsfUtil.addErrorMessage("Contraseña incorrecta");
        } else {
            JsfUtil.addErrorMessage("El usuario no existe");
        }
        return null;

    }
}
