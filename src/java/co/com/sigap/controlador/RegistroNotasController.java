package co.com.sigap.controlador;

import co.com.sigap.entidades.RegistroNotas;
import co.com.sigap.controlador.util.JsfUtil;
import co.com.sigap.controlador.util.JsfUtil.PersistAction;

import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@Named("registroNotasController")
@SessionScoped
public class RegistroNotasController implements Serializable {

    @EJB
    private co.com.sigap.controlador.RegistroNotasFacade ejbFacade;
    private List<RegistroNotas> items = null;
    private RegistroNotas selected;

    public RegistroNotasController() {
    }

    public RegistroNotas getSelected() {
        return selected;
    }

    public void setSelected(RegistroNotas selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
        selected.getRegistroNotasPK().setIdentificacionEstudiante(selected.getEstudiante().getCedulaEstudiante());
        selected.getRegistroNotasPK().setCodigoAsignatura(selected.getAsignatura().getCodigoAsignatura());
    }

    protected void initializeEmbeddableKey() {
        selected.setRegistroNotasPK(new co.com.sigap.entidades.RegistroNotasPK());
    }

    private RegistroNotasFacade getFacade() {
        return ejbFacade;
    }

    public RegistroNotas prepareCreate() {
        selected = new RegistroNotas();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("RegistroNotasCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("RegistroNotasUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("RegistroNotasDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<RegistroNotas> getItems() {
        //if (items == null) {
          items = getFacade().findAll();
        //}
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public RegistroNotas getRegistroNotas(co.com.sigap.entidades.RegistroNotasPK id) {
        return getFacade().find(id);
    }

    public List<RegistroNotas> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<RegistroNotas> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = RegistroNotas.class)
    public static class RegistroNotasControllerConverter implements Converter {

        private static final String SEPARATOR = "#";
        private static final String SEPARATOR_ESCAPED = "\\#";

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            RegistroNotasController controller = (RegistroNotasController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "registroNotasController");
            return controller.getRegistroNotas(getKey(value));
        }

        co.com.sigap.entidades.RegistroNotasPK getKey(String value) {
            co.com.sigap.entidades.RegistroNotasPK key;
            String values[] = value.split(SEPARATOR_ESCAPED);
            key = new co.com.sigap.entidades.RegistroNotasPK();
            key.setCodigoAsignatura(values[0]);
            key.setIdentificacionEstudiante(values[1]);
            return key;
        }

        String getStringKey(co.com.sigap.entidades.RegistroNotasPK value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value.getCodigoAsignatura());
            sb.append(SEPARATOR);
            sb.append(value.getIdentificacionEstudiante());
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof RegistroNotas) {
                RegistroNotas o = (RegistroNotas) object;
                return getStringKey(o.getRegistroNotasPK());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), RegistroNotas.class.getName()});
                return null;
            }
        }

    }

}
