package co.com.sigap.controlador;

import co.com.sigap.entidades.MaterialPorGrado;
import co.com.sigap.controlador.util.JsfUtil;
import co.com.sigap.controlador.util.JsfUtil.PersistAction;

import java.io.Serializable;
import java.util.List;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.inject.Named;
import javax.enterprise.context.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;

@Named("materialPorGradoController")
@SessionScoped
public class MaterialPorGradoController implements Serializable {

    @EJB
    private co.com.sigap.controlador.MaterialPorGradoFacade ejbFacade;
    private List<MaterialPorGrado> items = null;
    private MaterialPorGrado selected;

    public MaterialPorGradoController() {
    }

    public MaterialPorGrado getSelected() {
        return selected;
    }

    public void setSelected(MaterialPorGrado selected) {
        this.selected = selected;
    }

    protected void setEmbeddableKeys() {
        selected.getMaterialPorGradoPK().setMaterial(selected.getMaterial1().getIdMaterial());
        selected.getMaterialPorGradoPK().setGrado(selected.getGrado1().getIdGrado());
    }

    protected void initializeEmbeddableKey() {
        selected.setMaterialPorGradoPK(new co.com.sigap.entidades.MaterialPorGradoPK());
    }

    private MaterialPorGradoFacade getFacade() {
        return ejbFacade;
    }

    public MaterialPorGrado prepareCreate() {
        selected = new MaterialPorGrado();
        initializeEmbeddableKey();
        return selected;
    }

    public void create() {
        persist(PersistAction.CREATE, ResourceBundle.getBundle("/Bundle").getString("MaterialPorGradoCreated"));
        if (!JsfUtil.isValidationFailed()) {
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public void update() {
        persist(PersistAction.UPDATE, ResourceBundle.getBundle("/Bundle").getString("MaterialPorGradoUpdated"));
    }

    public void destroy() {
        persist(PersistAction.DELETE, ResourceBundle.getBundle("/Bundle").getString("MaterialPorGradoDeleted"));
        if (!JsfUtil.isValidationFailed()) {
            selected = null; // Remove selection
            items = null;    // Invalidate list of items to trigger re-query.
        }
    }

    public List<MaterialPorGrado> getItems() {
        //if (items == null) {
          items = getFacade().findAll();
        //}
        return items;
    }

    private void persist(PersistAction persistAction, String successMessage) {
        if (selected != null) {
            setEmbeddableKeys();
            try {
                if (persistAction != PersistAction.DELETE) {
                    getFacade().edit(selected);
                } else {
                    getFacade().remove(selected);
                }
                JsfUtil.addSuccessMessage(successMessage);
            } catch (EJBException ex) {
                String msg = "";
                Throwable cause = ex.getCause();
                if (cause != null) {
                    msg = cause.getLocalizedMessage();
                }
                if (msg.length() > 0) {
                    JsfUtil.addErrorMessage(msg);
                } else {
                    JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
                }
            } catch (Exception ex) {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, null, ex);
                JsfUtil.addErrorMessage(ex, ResourceBundle.getBundle("/Bundle").getString("PersistenceErrorOccured"));
            }
        }
    }

    public MaterialPorGrado getMaterialPorGrado(co.com.sigap.entidades.MaterialPorGradoPK id) {
        return getFacade().find(id);
    }

    public List<MaterialPorGrado> getItemsAvailableSelectMany() {
        return getFacade().findAll();
    }

    public List<MaterialPorGrado> getItemsAvailableSelectOne() {
        return getFacade().findAll();
    }

    @FacesConverter(forClass = MaterialPorGrado.class)
    public static class MaterialPorGradoControllerConverter implements Converter {

        private static final String SEPARATOR = "#";
        private static final String SEPARATOR_ESCAPED = "\\#";

        @Override
        public Object getAsObject(FacesContext facesContext, UIComponent component, String value) {
            if (value == null || value.length() == 0) {
                return null;
            }
            MaterialPorGradoController controller = (MaterialPorGradoController) facesContext.getApplication().getELResolver().
                    getValue(facesContext.getELContext(), null, "materialPorGradoController");
            return controller.getMaterialPorGrado(getKey(value));
        }

        co.com.sigap.entidades.MaterialPorGradoPK getKey(String value) {
            co.com.sigap.entidades.MaterialPorGradoPK key;
            String values[] = value.split(SEPARATOR_ESCAPED);
            key = new co.com.sigap.entidades.MaterialPorGradoPK();
            key.setGrado(values[0]);
            key.setMaterial(values[1]);
            return key;
        }

        String getStringKey(co.com.sigap.entidades.MaterialPorGradoPK value) {
            StringBuilder sb = new StringBuilder();
            sb.append(value.getGrado());
            sb.append(SEPARATOR);
            sb.append(value.getMaterial());
            return sb.toString();
        }

        @Override
        public String getAsString(FacesContext facesContext, UIComponent component, Object object) {
            if (object == null) {
                return null;
            }
            if (object instanceof MaterialPorGrado) {
                MaterialPorGrado o = (MaterialPorGrado) object;
                return getStringKey(o.getMaterialPorGradoPK());
            } else {
                Logger.getLogger(this.getClass().getName()).log(Level.SEVERE, "object {0} is of type {1}; expected type: {2}", new Object[]{object, object.getClass().getName(), MaterialPorGrado.class.getName()});
                return null;
            }
        }

    }

}
