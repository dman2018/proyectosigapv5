/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.sigap.controlador;

import co.com.sigap.controlador.util.JsfUtil;
import co.com.sigap.entidades.Estudiante;
import co.com.sigap.entidades.Grado;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.inject.Named;
import javax.enterprise.context.ApplicationScoped;

/**
 *
 * @author Auris
 */
@Named(value = "parametricasBean")
@ApplicationScoped
public class parametricasBean {

        private List<Estudiante> asignatura; 
        
        @EJB
        private EstudianteFacade conEstudiante;
        
        private String estudianteSeleccionado = "";
    /**
     * Creates a new instance of parametricasBean
     */
    public parametricasBean() {
    }
    
    @PostConstruct
    public void inicializar()
    {
     //   asignatura = new ArrayList<>();
     //   Estudiante juan = new Estudiante();
     //   juan.setCedulaEstudiante("1053767021");
     //   Grado primero = new Grado();
     //   primero.setIdGrado("1");
     //   primero.setNombreGrado("Primero");
     //   juan.setGrado(primero);
     //   asignatura.add(juan);
        asignatura = conEstudiante.obtenerAsignatura();
        
    }
     
    
    public List<Estudiante> getAsignatura() {
        return asignatura;
    }

    public void setAsignatura(List<Estudiante> asignatura) {
        this.asignatura = asignatura;
    }
    
    public String getEstudianteSeleccionado() {
        return estudianteSeleccionado;
    }

    public void setEstudianteSeleccionado(String estudianteSeleccionado) {
        this.estudianteSeleccionado = estudianteSeleccionado;
    }    
    
    public void muestreCambioEstudiante(){
        JsfUtil.addSuccessMessage("Estudiante seleccionado "+estudianteSeleccionado);
    }
    
}
