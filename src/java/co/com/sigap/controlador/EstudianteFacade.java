/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.sigap.controlador;

import co.com.sigap.entidades.Estudiante;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

/**
 *
 * @author Auris
 */
@Stateless
public class EstudianteFacade extends AbstractFacade<Estudiante> {

    @PersistenceContext(unitName = "sigapPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public EstudianteFacade() {
        super(Estudiante.class);
    }

    public List<Estudiante> obtenerAsignatura(){
        Query q = em.createNativeQuery("SELECT c.cedula_estudiante, a.codigo_asignatura"
                + "FROM estudiante c JOIN asignatura a ON c.grado = a.grado WHERE a.codigo_asignatura = 'a4'", Estudiante.class);
    
        return q.getResultList();
    }
}
    
    
