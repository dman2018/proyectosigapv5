/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.sigap.controlador;

import co.com.sigap.entidades.RegistroNotas;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Auris
 */
@Stateless
public class RegistroNotasFacade extends AbstractFacade<RegistroNotas> {

    @PersistenceContext(unitName = "sigapPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public RegistroNotasFacade() {
        super(RegistroNotas.class);
    }
    
}
