/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.sigap.entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Auris
 */
@Entity
@Table(name = "inscripcion")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Inscripcion.findAll", query = "SELECT i FROM Inscripcion i")
    , @NamedQuery(name = "Inscripcion.findByIdentificacion", query = "SELECT i FROM Inscripcion i WHERE i.identificacion = :identificacion")
    , @NamedQuery(name = "Inscripcion.findByPrimerNombre", query = "SELECT i FROM Inscripcion i WHERE i.primerNombre = :primerNombre")
    , @NamedQuery(name = "Inscripcion.findBySegundoNombre", query = "SELECT i FROM Inscripcion i WHERE i.segundoNombre = :segundoNombre")
    , @NamedQuery(name = "Inscripcion.findByPrimerApellido", query = "SELECT i FROM Inscripcion i WHERE i.primerApellido = :primerApellido")
    , @NamedQuery(name = "Inscripcion.findBySegundoApellido", query = "SELECT i FROM Inscripcion i WHERE i.segundoApellido = :segundoApellido")
    , @NamedQuery(name = "Inscripcion.findByDireccion", query = "SELECT i FROM Inscripcion i WHERE i.direccion = :direccion")
    , @NamedQuery(name = "Inscripcion.findByTelefono", query = "SELECT i FROM Inscripcion i WHERE i.telefono = :telefono")
    , @NamedQuery(name = "Inscripcion.findByFechaNacimiento", query = "SELECT i FROM Inscripcion i WHERE i.fechaNacimiento = :fechaNacimiento")
    , @NamedQuery(name = "Inscripcion.findByEmail", query = "SELECT i FROM Inscripcion i WHERE i.email = :email")
    , @NamedQuery(name = "Inscripcion.findByIdentificacionAcudiente", query = "SELECT i FROM Inscripcion i WHERE i.identificacionAcudiente = :identificacionAcudiente")
    , @NamedQuery(name = "Inscripcion.findByNombreAcudiente", query = "SELECT i FROM Inscripcion i WHERE i.nombreAcudiente = :nombreAcudiente")
    , @NamedQuery(name = "Inscripcion.findByApellidoAcudiente", query = "SELECT i FROM Inscripcion i WHERE i.apellidoAcudiente = :apellidoAcudiente")
    , @NamedQuery(name = "Inscripcion.findByDireccionAcudiente", query = "SELECT i FROM Inscripcion i WHERE i.direccionAcudiente = :direccionAcudiente")
    , @NamedQuery(name = "Inscripcion.findByTelefonoAcudiente", query = "SELECT i FROM Inscripcion i WHERE i.telefonoAcudiente = :telefonoAcudiente")
    , @NamedQuery(name = "Inscripcion.findByEmailAcudiente", query = "SELECT i FROM Inscripcion i WHERE i.emailAcudiente = :emailAcudiente")
    , @NamedQuery(name = "Inscripcion.findByGradoCursar", query = "SELECT i FROM Inscripcion i WHERE i.gradoCursar = :gradoCursar")})
public class Inscripcion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "identificacion")
    private String identificacion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "primer_nombre")
    private String primerNombre;
    @Size(max = 15)
    @Column(name = "segundo_nombre")
    private String segundoNombre;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "primer_apellido")
    private String primerApellido;
    @Size(max = 15)
    @Column(name = "segundo_apellido")
    private String segundoApellido;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 30)
    @Column(name = "direccion")
    private String direccion;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "telefono")
    private String telefono;
    @Basic(optional = false)
    @NotNull
    @Column(name = "fecha_nacimiento")
    @Temporal(TemporalType.DATE)
    private Date fechaNacimiento;
    // @Pattern(regexp="[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?", message="Invalid email")//if the field contains email address consider using this annotation to enforce field validation
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "email")
    private String email;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "identificacion_acudiente")
    private String identificacionAcudiente;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "nombre_acudiente")
    private String nombreAcudiente;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "apellido_acudiente")
    private String apellidoAcudiente;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "direccion_acudiente")
    private String direccionAcudiente;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "telefono_acudiente")
    private String telefonoAcudiente;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 40)
    @Column(name = "email_acudiente")
    private String emailAcudiente;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "grado_cursar")
    private String gradoCursar;

    public Inscripcion() {
    }

    public Inscripcion(String identificacion) {
        this.identificacion = identificacion;
    }

    public Inscripcion(String identificacion, String primerNombre, String primerApellido, String direccion, String telefono, Date fechaNacimiento, String email, String identificacionAcudiente, String nombreAcudiente, String apellidoAcudiente, String direccionAcudiente, String telefonoAcudiente, String emailAcudiente, String gradoCursar) {
        this.identificacion = identificacion;
        this.primerNombre = primerNombre;
        this.primerApellido = primerApellido;
        this.direccion = direccion;
        this.telefono = telefono;
        this.fechaNacimiento = fechaNacimiento;
        this.email = email;
        this.identificacionAcudiente = identificacionAcudiente;
        this.nombreAcudiente = nombreAcudiente;
        this.apellidoAcudiente = apellidoAcudiente;
        this.direccionAcudiente = direccionAcudiente;
        this.telefonoAcudiente = telefonoAcudiente;
        this.emailAcudiente = emailAcudiente;
        this.gradoCursar = gradoCursar;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getPrimerNombre() {
        return primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    public String getSegundoNombre() {
        return segundoNombre;
    }

    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Date getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIdentificacionAcudiente() {
        return identificacionAcudiente;
    }

    public void setIdentificacionAcudiente(String identificacionAcudiente) {
        this.identificacionAcudiente = identificacionAcudiente;
    }

    public String getNombreAcudiente() {
        return nombreAcudiente;
    }

    public void setNombreAcudiente(String nombreAcudiente) {
        this.nombreAcudiente = nombreAcudiente;
    }

    public String getApellidoAcudiente() {
        return apellidoAcudiente;
    }

    public void setApellidoAcudiente(String apellidoAcudiente) {
        this.apellidoAcudiente = apellidoAcudiente;
    }

    public String getDireccionAcudiente() {
        return direccionAcudiente;
    }

    public void setDireccionAcudiente(String direccionAcudiente) {
        this.direccionAcudiente = direccionAcudiente;
    }

    public String getTelefonoAcudiente() {
        return telefonoAcudiente;
    }

    public void setTelefonoAcudiente(String telefonoAcudiente) {
        this.telefonoAcudiente = telefonoAcudiente;
    }

    public String getEmailAcudiente() {
        return emailAcudiente;
    }

    public void setEmailAcudiente(String emailAcudiente) {
        this.emailAcudiente = emailAcudiente;
    }

    public String getGradoCursar() {
        return gradoCursar;
    }

    public void setGradoCursar(String gradoCursar) {
        this.gradoCursar = gradoCursar;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (identificacion != null ? identificacion.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Inscripcion)) {
            return false;
        }
        Inscripcion other = (Inscripcion) object;
        if ((this.identificacion == null && other.identificacion != null) || (this.identificacion != null && !this.identificacion.equals(other.identificacion))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.com.sigap.entidades.Inscripcion[ identificacion=" + identificacion + " ]";
    }
    
}
