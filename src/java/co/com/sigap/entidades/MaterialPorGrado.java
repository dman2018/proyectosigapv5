/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.sigap.entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Auris
 */
@Entity
@Table(name = "material_por_grado")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "MaterialPorGrado.findAll", query = "SELECT m FROM MaterialPorGrado m")
    , @NamedQuery(name = "MaterialPorGrado.findByGrado", query = "SELECT m FROM MaterialPorGrado m WHERE m.materialPorGradoPK.grado = :grado")
    , @NamedQuery(name = "MaterialPorGrado.findByMaterial", query = "SELECT m FROM MaterialPorGrado m WHERE m.materialPorGradoPK.material = :material")
    , @NamedQuery(name = "MaterialPorGrado.findByCantidadMaterial", query = "SELECT m FROM MaterialPorGrado m WHERE m.cantidadMaterial = :cantidadMaterial")})
public class MaterialPorGrado implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected MaterialPorGradoPK materialPorGradoPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "cantidad_material")
    private int cantidadMaterial;
    @JoinColumn(name = "grado", referencedColumnName = "id_grado", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Grado grado1;
    @JoinColumn(name = "material", referencedColumnName = "id_material", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Material material1;

    public MaterialPorGrado() {
    }

    public MaterialPorGrado(MaterialPorGradoPK materialPorGradoPK) {
        this.materialPorGradoPK = materialPorGradoPK;
    }

    public MaterialPorGrado(MaterialPorGradoPK materialPorGradoPK, int cantidadMaterial) {
        this.materialPorGradoPK = materialPorGradoPK;
        this.cantidadMaterial = cantidadMaterial;
    }

    public MaterialPorGrado(String grado, String material) {
        this.materialPorGradoPK = new MaterialPorGradoPK(grado, material);
    }

    public MaterialPorGradoPK getMaterialPorGradoPK() {
        return materialPorGradoPK;
    }

    public void setMaterialPorGradoPK(MaterialPorGradoPK materialPorGradoPK) {
        this.materialPorGradoPK = materialPorGradoPK;
    }

    public int getCantidadMaterial() {
        return cantidadMaterial;
    }

    public void setCantidadMaterial(int cantidadMaterial) {
        this.cantidadMaterial = cantidadMaterial;
    }

    public Grado getGrado1() {
        return grado1;
    }

    public void setGrado1(Grado grado1) {
        this.grado1 = grado1;
    }

    public Material getMaterial1() {
        return material1;
    }

    public void setMaterial1(Material material1) {
        this.material1 = material1;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (materialPorGradoPK != null ? materialPorGradoPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MaterialPorGrado)) {
            return false;
        }
        MaterialPorGrado other = (MaterialPorGrado) object;
        if ((this.materialPorGradoPK == null && other.materialPorGradoPK != null) || (this.materialPorGradoPK != null && !this.materialPorGradoPK.equals(other.materialPorGradoPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.com.sigap.entidades.MaterialPorGrado[ materialPorGradoPK=" + materialPorGradoPK + " ]";
    }
    
}
