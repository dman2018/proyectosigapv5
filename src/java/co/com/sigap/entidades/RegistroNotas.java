/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.sigap.entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Auris
 */
@Entity
@Table(name = "registro_notas")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "RegistroNotas.findAll", query = "SELECT r FROM RegistroNotas r")
    , @NamedQuery(name = "RegistroNotas.findByCodigoAsignatura", query = "SELECT r FROM RegistroNotas r WHERE r.registroNotasPK.codigoAsignatura = :codigoAsignatura")
    , @NamedQuery(name = "RegistroNotas.findByIdentificacionEstudiante", query = "SELECT r FROM RegistroNotas r WHERE r.registroNotasPK.identificacionEstudiante = :identificacionEstudiante")
    , @NamedQuery(name = "RegistroNotas.findByNotaUno", query = "SELECT r FROM RegistroNotas r WHERE r.notaUno = :notaUno")
    , @NamedQuery(name = "RegistroNotas.findByNotaDos", query = "SELECT r FROM RegistroNotas r WHERE r.notaDos = :notaDos")
    , @NamedQuery(name = "RegistroNotas.findByNotaTres", query = "SELECT r FROM RegistroNotas r WHERE r.notaTres = :notaTres")
    , @NamedQuery(name = "RegistroNotas.findByNotaCuatro", query = "SELECT r FROM RegistroNotas r WHERE r.notaCuatro = :notaCuatro")
    , @NamedQuery(name = "RegistroNotas.findByNotaQuinto", query = "SELECT r FROM RegistroNotas r WHERE r.notaQuinto = :notaQuinto")})
public class RegistroNotas implements Serializable {

    private static final long serialVersionUID = 1L;
    @EmbeddedId
    protected RegistroNotasPK registroNotasPK;
    @Basic(optional = false)
    @NotNull
    @Column(name = "nota_uno")
    private double notaUno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "nota_dos")
    private double notaDos;
    @Basic(optional = false)
    @NotNull
    @Column(name = "nota_tres")
    private double notaTres;
    @Basic(optional = false)
    @NotNull
    @Column(name = "nota_cuatro")
    private double notaCuatro;
    @Basic(optional = false)
    @NotNull
    @Column(name = "nota_quinto")
    private double notaQuinto;
    @JoinColumn(name = "codigo_asignatura", referencedColumnName = "codigo_asignatura", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Asignatura asignatura;
    @JoinColumn(name = "identificacion_estudiante", referencedColumnName = "cedula_estudiante", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Estudiante estudiante;

    public RegistroNotas() {
    }

    public RegistroNotas(RegistroNotasPK registroNotasPK) {
        this.registroNotasPK = registroNotasPK;
    }

    public RegistroNotas(RegistroNotasPK registroNotasPK, double notaUno, double notaDos, double notaTres, double notaCuatro, double notaQuinto) {
        this.registroNotasPK = registroNotasPK;
        this.notaUno = notaUno;
        this.notaDos = notaDos;
        this.notaTres = notaTres;
        this.notaCuatro = notaCuatro;
        this.notaQuinto = notaQuinto;
    }

    public RegistroNotas(String codigoAsignatura, String identificacionEstudiante) {
        this.registroNotasPK = new RegistroNotasPK(codigoAsignatura, identificacionEstudiante);
    }

    public RegistroNotasPK getRegistroNotasPK() {
        return registroNotasPK;
    }

    public void setRegistroNotasPK(RegistroNotasPK registroNotasPK) {
        this.registroNotasPK = registroNotasPK;
    }

    public double getNotaUno() {
        return notaUno;
    }

    public void setNotaUno(double notaUno) {
        this.notaUno = notaUno;
    }

    public double getNotaDos() {
        return notaDos;
    }

    public void setNotaDos(double notaDos) {
        this.notaDos = notaDos;
    }

    public double getNotaTres() {
        return notaTres;
    }

    public void setNotaTres(double notaTres) {
        this.notaTres = notaTres;
    }

    public double getNotaCuatro() {
        return notaCuatro;
    }

    public void setNotaCuatro(double notaCuatro) {
        this.notaCuatro = notaCuatro;
    }

    public double getNotaQuinto() {
        return notaQuinto;
    }

    public void setNotaQuinto(double notaQuinto) {
        this.notaQuinto = notaQuinto;
    }

    public Asignatura getAsignatura() {
        return asignatura;
    }

    public void setAsignatura(Asignatura asignatura) {
        this.asignatura = asignatura;
    }

    public Estudiante getEstudiante() {
        return estudiante;
    }

    public void setEstudiante(Estudiante estudiante) {
        this.estudiante = estudiante;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (registroNotasPK != null ? registroNotasPK.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RegistroNotas)) {
            return false;
        }
        RegistroNotas other = (RegistroNotas) object;
        if ((this.registroNotasPK == null && other.registroNotasPK != null) || (this.registroNotasPK != null && !this.registroNotasPK.equals(other.registroNotasPK))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.com.sigap.entidades.RegistroNotas[ registroNotasPK=" + registroNotasPK + " ]";
    }
    
}
