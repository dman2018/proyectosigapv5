/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.sigap.entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Auris
 */
@Embeddable
public class MaterialPorGradoPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "grado")
    private String grado;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "material")
    private String material;

    public MaterialPorGradoPK() {
    }

    public MaterialPorGradoPK(String grado, String material) {
        this.grado = grado;
        this.material = material;
    }

    public String getGrado() {
        return grado;
    }

    public void setGrado(String grado) {
        this.grado = grado;
    }

    public String getMaterial() {
        return material;
    }

    public void setMaterial(String material) {
        this.material = material;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (grado != null ? grado.hashCode() : 0);
        hash += (material != null ? material.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MaterialPorGradoPK)) {
            return false;
        }
        MaterialPorGradoPK other = (MaterialPorGradoPK) object;
        if ((this.grado == null && other.grado != null) || (this.grado != null && !this.grado.equals(other.grado))) {
            return false;
        }
        if ((this.material == null && other.material != null) || (this.material != null && !this.material.equals(other.material))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.com.sigap.entidades.MaterialPorGradoPK[ grado=" + grado + ", material=" + material + " ]";
    }
    
}
