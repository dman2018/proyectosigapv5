/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.sigap.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Auris
 */
@Entity
@Table(name = "asignatura")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Asignatura.findAll", query = "SELECT a FROM Asignatura a")
    , @NamedQuery(name = "Asignatura.findByCodigoAsignatura", query = "SELECT a FROM Asignatura a WHERE a.codigoAsignatura = :codigoAsignatura")
    , @NamedQuery(name = "Asignatura.findByNombreAsignatura", query = "SELECT a FROM Asignatura a WHERE a.nombreAsignatura = :nombreAsignatura")
    , @NamedQuery(name = "Asignatura.findByDescripcionAsignatura", query = "SELECT a FROM Asignatura a WHERE a.descripcionAsignatura = :descripcionAsignatura")
    , @NamedQuery(name = "Asignatura.findByPorcentajeNotaUno", query = "SELECT a FROM Asignatura a WHERE a.porcentajeNotaUno = :porcentajeNotaUno")
    , @NamedQuery(name = "Asignatura.findByPorcentajeNotaDos", query = "SELECT a FROM Asignatura a WHERE a.porcentajeNotaDos = :porcentajeNotaDos")
    , @NamedQuery(name = "Asignatura.findByPorcentajeNotaTres", query = "SELECT a FROM Asignatura a WHERE a.porcentajeNotaTres = :porcentajeNotaTres")
    , @NamedQuery(name = "Asignatura.findByPorcentajeNotaCuatro", query = "SELECT a FROM Asignatura a WHERE a.porcentajeNotaCuatro = :porcentajeNotaCuatro")
    , @NamedQuery(name = "Asignatura.findByPorcentajeNotaCinco", query = "SELECT a FROM Asignatura a WHERE a.porcentajeNotaCinco = :porcentajeNotaCinco")})
public class Asignatura implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "codigo_asignatura")
    private String codigoAsignatura;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "nombre_asignatura")
    private String nombreAsignatura;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 20)
    @Column(name = "descripcion_asignatura")
    private String descripcionAsignatura;
    @Basic(optional = false)
    @NotNull
    @Column(name = "porcentaje_nota_uno")
    private double porcentajeNotaUno;
    @Basic(optional = false)
    @NotNull
    @Column(name = "porcentaje_nota_dos")
    private double porcentajeNotaDos;
    @Basic(optional = false)
    @NotNull
    @Column(name = "porcentaje_nota_tres")
    private double porcentajeNotaTres;
    @Basic(optional = false)
    @NotNull
    @Column(name = "porcentaje_nota_cuatro")
    private double porcentajeNotaCuatro;
    @Basic(optional = false)
    @NotNull
    @Column(name = "porcentaje_nota_cinco")
    private double porcentajeNotaCinco;
    @JoinColumn(name = "identificacion_docente", referencedColumnName = "identificacion_docente")
    @ManyToOne(optional = false)
    private Docente identificacionDocente;
    @JoinColumn(name = "grado", referencedColumnName = "id_grado")
    @ManyToOne(optional = false)
    private Grado grado;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "asignatura")
    private List<RegistroNotas> registroNotasList;

    public Asignatura() {
    }

    public Asignatura(String codigoAsignatura) {
        this.codigoAsignatura = codigoAsignatura;
    }

    public Asignatura(String codigoAsignatura, String nombreAsignatura, String descripcionAsignatura, double porcentajeNotaUno, double porcentajeNotaDos, double porcentajeNotaTres, double porcentajeNotaCuatro, double porcentajeNotaCinco) {
        this.codigoAsignatura = codigoAsignatura;
        this.nombreAsignatura = nombreAsignatura;
        this.descripcionAsignatura = descripcionAsignatura;
        this.porcentajeNotaUno = porcentajeNotaUno;
        this.porcentajeNotaDos = porcentajeNotaDos;
        this.porcentajeNotaTres = porcentajeNotaTres;
        this.porcentajeNotaCuatro = porcentajeNotaCuatro;
        this.porcentajeNotaCinco = porcentajeNotaCinco;
    }

    public String getCodigoAsignatura() {
        return codigoAsignatura;
    }

    public void setCodigoAsignatura(String codigoAsignatura) {
        this.codigoAsignatura = codigoAsignatura;
    }

    public String getNombreAsignatura() {
        return nombreAsignatura;
    }

    public void setNombreAsignatura(String nombreAsignatura) {
        this.nombreAsignatura = nombreAsignatura;
    }

    public String getDescripcionAsignatura() {
        return descripcionAsignatura;
    }

    public void setDescripcionAsignatura(String descripcionAsignatura) {
        this.descripcionAsignatura = descripcionAsignatura;
    }

    public double getPorcentajeNotaUno() {
        return porcentajeNotaUno;
    }

    public void setPorcentajeNotaUno(double porcentajeNotaUno) {
        this.porcentajeNotaUno = porcentajeNotaUno;
    }

    public double getPorcentajeNotaDos() {
        return porcentajeNotaDos;
    }

    public void setPorcentajeNotaDos(double porcentajeNotaDos) {
        this.porcentajeNotaDos = porcentajeNotaDos;
    }

    public double getPorcentajeNotaTres() {
        return porcentajeNotaTres;
    }

    public void setPorcentajeNotaTres(double porcentajeNotaTres) {
        this.porcentajeNotaTres = porcentajeNotaTres;
    }

    public double getPorcentajeNotaCuatro() {
        return porcentajeNotaCuatro;
    }

    public void setPorcentajeNotaCuatro(double porcentajeNotaCuatro) {
        this.porcentajeNotaCuatro = porcentajeNotaCuatro;
    }

    public double getPorcentajeNotaCinco() {
        return porcentajeNotaCinco;
    }

    public void setPorcentajeNotaCinco(double porcentajeNotaCinco) {
        this.porcentajeNotaCinco = porcentajeNotaCinco;
    }

    public Docente getIdentificacionDocente() {
        return identificacionDocente;
    }

    public void setIdentificacionDocente(Docente identificacionDocente) {
        this.identificacionDocente = identificacionDocente;
    }

    public Grado getGrado() {
        return grado;
    }

    public void setGrado(Grado grado) {
        this.grado = grado;
    }

    @XmlTransient
    public List<RegistroNotas> getRegistroNotasList() {
        return registroNotasList;
    }

    public void setRegistroNotasList(List<RegistroNotas> registroNotasList) {
        this.registroNotasList = registroNotasList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigoAsignatura != null ? codigoAsignatura.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Asignatura)) {
            return false;
        }
        Asignatura other = (Asignatura) object;
        if ((this.codigoAsignatura == null && other.codigoAsignatura != null) || (this.codigoAsignatura != null && !this.codigoAsignatura.equals(other.codigoAsignatura))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.nombreAsignatura;
    }
    
}
