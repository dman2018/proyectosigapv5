/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.sigap.entidades;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Auris
 */
@Entity
@Table(name = "grado")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Grado.findAll", query = "SELECT g FROM Grado g")
    , @NamedQuery(name = "Grado.findByIdGrado", query = "SELECT g FROM Grado g WHERE g.idGrado = :idGrado")
    , @NamedQuery(name = "Grado.findByNombreGrado", query = "SELECT g FROM Grado g WHERE g.nombreGrado = :nombreGrado")
    , @NamedQuery(name = "Grado.findByNumeroMaximoEstudiantes", query = "SELECT g FROM Grado g WHERE g.numeroMaximoEstudiantes = :numeroMaximoEstudiantes")})
public class Grado implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 10)
    @Column(name = "id_grado")
    private String idGrado;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "nombre_grado")
    private String nombreGrado;
    @Basic(optional = false)
    @NotNull
    @Column(name = "numero_maximo_estudiantes")
    private int numeroMaximoEstudiantes;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "grado")
    private List<Estudiante> estudianteList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "grado")
    private List<Asignatura> asignaturaList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "grado1")
    private List<MaterialPorGrado> materialPorGradoList;

    public Grado() {
    }

    public Grado(String idGrado) {
        this.idGrado = idGrado;
    }

    public Grado(String idGrado, String nombreGrado, int numeroMaximoEstudiantes) {
        this.idGrado = idGrado;
        this.nombreGrado = nombreGrado;
        this.numeroMaximoEstudiantes = numeroMaximoEstudiantes;
    }

    public String getIdGrado() {
        return idGrado;
    }

    public void setIdGrado(String idGrado) {
        this.idGrado = idGrado;
    }

    public String getNombreGrado() {
        return nombreGrado;
    }

    public void setNombreGrado(String nombreGrado) {
        this.nombreGrado = nombreGrado;
    }

    public int getNumeroMaximoEstudiantes() {
        return numeroMaximoEstudiantes;
    }

    public void setNumeroMaximoEstudiantes(int numeroMaximoEstudiantes) {
        this.numeroMaximoEstudiantes = numeroMaximoEstudiantes;
    }

    @XmlTransient
    public List<Estudiante> getEstudianteList() {
        return estudianteList;
    }

    public void setEstudianteList(List<Estudiante> estudianteList) {
        this.estudianteList = estudianteList;
    }

    @XmlTransient
    public List<Asignatura> getAsignaturaList() {
        return asignaturaList;
    }

    public void setAsignaturaList(List<Asignatura> asignaturaList) {
        this.asignaturaList = asignaturaList;
    }

    @XmlTransient
    public List<MaterialPorGrado> getMaterialPorGradoList() {
        return materialPorGradoList;
    }

    public void setMaterialPorGradoList(List<MaterialPorGrado> materialPorGradoList) {
        this.materialPorGradoList = materialPorGradoList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idGrado != null ? idGrado.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Grado)) {
            return false;
        }
        Grado other = (Grado) object;
        if ((this.idGrado == null && other.idGrado != null) || (this.idGrado != null && !this.idGrado.equals(other.idGrado))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return this.nombreGrado;
    }
    
}
