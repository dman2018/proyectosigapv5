/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.com.sigap.entidades;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 *
 * @author Auris
 */
@Embeddable
public class RegistroNotasPK implements Serializable {

    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "codigo_asignatura")
    private String codigoAsignatura;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 15)
    @Column(name = "identificacion_estudiante")
    private String identificacionEstudiante;

    public RegistroNotasPK() {
    }

    public RegistroNotasPK(String codigoAsignatura, String identificacionEstudiante) {
        this.codigoAsignatura = codigoAsignatura;
        this.identificacionEstudiante = identificacionEstudiante;
    }

    public String getCodigoAsignatura() {
        return codigoAsignatura;
    }

    public void setCodigoAsignatura(String codigoAsignatura) {
        this.codigoAsignatura = codigoAsignatura;
    }

    public String getIdentificacionEstudiante() {
        return identificacionEstudiante;
    }

    public void setIdentificacionEstudiante(String identificacionEstudiante) {
        this.identificacionEstudiante = identificacionEstudiante;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigoAsignatura != null ? codigoAsignatura.hashCode() : 0);
        hash += (identificacionEstudiante != null ? identificacionEstudiante.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof RegistroNotasPK)) {
            return false;
        }
        RegistroNotasPK other = (RegistroNotasPK) object;
        if ((this.codigoAsignatura == null && other.codigoAsignatura != null) || (this.codigoAsignatura != null && !this.codigoAsignatura.equals(other.codigoAsignatura))) {
            return false;
        }
        if ((this.identificacionEstudiante == null && other.identificacionEstudiante != null) || (this.identificacionEstudiante != null && !this.identificacionEstudiante.equals(other.identificacionEstudiante))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "co.com.sigap.entidades.RegistroNotasPK[ codigoAsignatura=" + codigoAsignatura + ", identificacionEstudiante=" + identificacionEstudiante + " ]";
    }
    
}
